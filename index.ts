import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";

const config = new pulumi.Config();
const vpcCidrBlock = config.require("vpcCidrBlock")
const publicCidrBlock = config.require("publicCidrBlock")
const privateCidrBlock = config.require("privateCidrBlock")

// Create an AWS resource (S3 Bucket)
const bucket = new aws.s3.Bucket("my-bucket");
const vpc = new aws.ec2.Vpc("vpc-a", {
    cidrBlock: vpcCidrBlock,
    enableDnsSupport: true,
    enableDnsHostnames: true,
    instanceTenancy: "default",
    tags: {
        Name: "vpc-a",
        },
});
const subnet1 = new aws.ec2.Subnet("public", {
    vpcId: vpc.id,
    cidrBlock: publicCidrBlock,
    availabilityZone: "us-east-1a",
    mapPublicIpOnLaunch: true,
    tags: {
        Name: "public",
        },
});
const subnet2 = new aws.ec2.Subnet("private", {
    vpcId: vpc.id,
    cidrBlock: privateCidrBlock,
    availabilityZone: "us-east-1b",
    mapPublicIpOnLaunch: true,
    tags: {
        Name: "private",
    }
});
const internetGateway = new aws.ec2.InternetGateway("igw", {
    vpcId: vpc.id,
    tags: {
        Name: "igw",
        },
});
const routeTable = new aws.ec2.RouteTable("rtb", {
    vpcId: vpc.id,
    tags: {
        Name: "rtb",
        },
});
const route = new aws.ec2.Route("rt", {
    routeTableId: routeTable.id,
    destinationCidrBlock: "0.0.0.0/0",
    gatewayId: internetGateway.id,
    }
);

// Create a security group allowing inbound access over port 80 and outbound
// access to anywhere.
const securityGroup1 = new aws.ec2.SecurityGroup("security-group", {
    vpcId: vpc.id,
    ingress: [
        {
            cidrBlocks: [ "0.0.0.0/0" ],
            protocol: "tcp",
            fromPort: 80,
            toPort: 80,
        },
    ],
    
    egress: [
        {
            cidrBlocks: [ "0.0.0.0/0" ],
            fromPort: 0,
            toPort: 0,
            protocol: "-1",
        },
    ],
});

const securityGroup2 = new aws.ec2.SecurityGroup("security-group2", {
    vpcId: vpc.id,
    ingress: [
        {
            cidrBlocks: [ "0.0.0.0/0" ],
            protocol: "tcp",
            fromPort: 22,
            toPort: 22,
        },
    ],
    
    egress: [
        {
            cidrBlocks: [ "0.0.0.0/0" ],
            fromPort: 0,
            toPort: 0,
            protocol: "-1",
        },
    ],
});
const instance1 = new aws.ec2.Instance("instance-1", {
    ami: "ami-006dcf34c09e50022",
    instanceType: "t3.micro",
    subnetId: subnet1.id,
    vpcSecurityGroupIds: [
        securityGroup1.id,
    ],
    userData: `
        #!/bin/bash
        amazon-linux-extras install nginx1
        amazon-linux-extras enable nginx
        systemctl enable nginx
        systemctl start nginx
    `,
});const instance2 = new aws.ec2.Instance("instance-2", {
    ami: "ami-006dcf34c09e50022",
    instanceType: "t3.micro",
    // keyName: 
    subnetId: subnet2.id,
    vpcSecurityGroupIds: [
        securityGroup2.id,
    ],
    userData: `
        #!/bin/bash
        yum install httpd
        systemctl start httpd
        systemctl enable httpd
    `,
});

const dashboard = new aws.cloudwatch.Dashboard("mydashboard", {
    dashboardName: "my-dashboard",
    dashboardBody: JSON.stringify({
        widgets: [
            {
                type: "metric",
                x: 0,
                y: 0,
                width: 12,
                height: 6,
                properties: {
                    metrics: [
                        [
                            "AWS/EC2",
                            "CPUUtilization",
                            {
                                "label": "Instance1",
                                "id": "i-042a89bf0712c2213"
                            }
                        ],
                        [
                            "AWS/EC2",
                            "CPUUtilization",
                            {
                                "label": "Instance2",
                                "id": "i-0dcab981f9bd84211"
                            }
                        ]
                    ],
                    period: 300,
                    stat: "Average",
                    region: "us-east-1",
                    title: "EC2 Instance CPU"
                }
            },
            {
                type: "text",
                x: 0,
                y: 7,
                width: 3,
                height: 3,
                properties: {
                    markdown: "Hello world"
                }
            }
        ]
    })
});

const lb = new aws.lb.LoadBalancer("alb-n", {
    name: "my-alb",
    subnets: [subnet1.id, subnet2.id],
    securityGroups: [securityGroup1.id, securityGroup2.id],
    
})


export const  vpcId = vpc.id;
export const  subnet1Id = subnet1.id;
export const  subnet2Id = subnet2.id;
export const instance1Id = instance1.id;
export const instance2Id = instance2.id;




